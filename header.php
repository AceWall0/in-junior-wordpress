<!-- Quem tiver feito o header no html, depois troca os códigos aqui. -->
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <?php wp_head()?>

</head>
<body>
<header>

    <figure>
        <img src="<?php echo get_template_directory_uri() ?>/img/logo-branca.png" alt="logo da InJ">
    </figure>
    <?php 
    $args = array(
        'menu' => 'menu',
        'container_class' => 'menu',
        'container' => 'nav'
    );
    wp_nav_menu($args);
    ?>
    <!-- <nav>
        <ul class="menu">    
            <li><a href="">HOME</a></li>
            <li><a href="">SOBRE NÓS</a></li>
            <li><a href="">CONTATO</a></li>
            <li><a href="">PRODUTOS</a></li>
            <li><a href="">NOTÍCIAS</a></li>
        </ul>
    </nav> -->
</header>