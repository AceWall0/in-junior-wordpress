<?php // Template Name: Home ?>

<?php get_header()?>

<main>
    <section id="intro">

    </section>
    <section id="sobre">

    </section>
    <section id="produtos">
        <div class="container">
            <div class="card">
                <img src="https://loremflickr.com/320/240/product?random=1">
                <h3>Um produto muito legal</h3>
            </div>
            <div class="card">
                <img src="https://loremflickr.com/320/240/product?random=2">
                <h3>Um produto muito legal</h3>
            </div>
            <div class="card">
                <img src="https://loremflickr.com/320/240/product?random=3">
                <h3>Um produto muito legal</h3>
            </div>
            <div class="card">
                <img src="https://loremflickr.com/320/240/product?random=4">
                <h3>Um produto muito legal</h3>
            </div>
        </div>
    </section>
    <section id="noticias">

    </section>
</main>

<?php get_footer()?>