<?php 
// Template Name: Sobre
?>

<?php get_header(); ?>

<img src="https://loremflickr.com/320/240/product?random=1" alt="Imagem descritiva">
<p> PARAGRAFO DE TESTE</p>
<nav>
    <?php if(get_field('facebook-url')): ?>
        <a href="<?php the_field('facebook-url') ?>">
        <img src="<?php echo get_template_directory_uri()?>/img/facebook.png" >
        </a>
    <?php endif ?>
    <?php if(get_field('instagram-url')): ?>
        <a href="<?php the_field('instagram-url') ?>">
        <img src="<?php echo get_template_directory_uri()?>/img/insta.png" >
        </a>
    <?php endif ?>
    <?php if(get_field('twitter-url')): ?>
        <a href="<?php the_field('twitter-url') ?>">
        <img src="<?php echo get_template_directory_uri()?>/img/twitter.png" >
        </a>
    <?php endif ?>
</nav>


<?php get_footer(); ?>