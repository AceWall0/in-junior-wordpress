<?php 
// Template Name: Notícias
?>
<?php get_header(); ?>

<main>
    <?php 
    $news = new WP_Query(
        array(
            'posts_per_page' => 5,
            'category_name' => 'noticia',
        )
    );

    if ($news->have_posts()):
        while ($news->have_posts()): 
            $news->the_post(); ?>
            <article>
                <h2><?php the_title(); ?></h2>
                <div><?php the_content(); ?></div>
            </article>
        <?php
        endwhile;
    else:
        echo "<p>Sem notícias<p>";
    endif;
    ?>

</main>

<?php get_footer(); ?>